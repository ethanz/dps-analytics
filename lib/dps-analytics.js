#! /usr/bin/env node

"use strict";

var	fs = require('fs');
var path = require('path');
var xlsx = require('node-xlsx');
var	parse = require('csv-parse');
var _ = require('underscore');

var outPutFileName = process.argv[2] || 'Analytics';

// Read the csv filenames from given path
fs.readdir('./', function(err, fileNames) {
	fileNames = fileNames.filter(function(fileName) {
		return path.extname(fileName) === '.csv';
	});
	formFileNameObject(fileNames);
});

// Form a object that contains different sets of file names - e.q. "Application Info", "Mobile", etc.
function formFileNameObject(fileNames) {
	createXLSX(_.groupBy(fileNames, function(value) {
		return value.split(' - ')[0];
	}));
}

// Write the XLSX file
function createXLSX(fileNames) {
	var csvData = [];
	var temp = [];
	// Convert filenames object into array
	fileNames = _.toArray(fileNames);

	// Function to process each file and build the xlsx file when finished
	function processDataSheet(index) {
		if (index < fileNames.length) {
			var startsWith = fileNames[index][0].split(' - ')[0];
			if (startsWith === 'Articles' || startsWith === 'Folios') {
				processArticlesData(index, 0);
			} else {
				processOtherData(index, 0);
			}
		} else {
			console.log('Writing spreadsheet...');
			var buffer = xlsx.build(csvData);
			fs.writeFileSync('./' + outPutFileName + '.xlsx', buffer);
			console.log('Finished creating "'+ outPutFileName + '.xlsx"');
		}
	}

	// Function to process files starting with "Articles" & "Folios" (creating separated datasheets)
	function processArticlesData(i, j) {
		if (j < fileNames[i].length) {
			var parser = parse({delimiter: ','}, function(err, data){
				console.log('Processing file: "' + fileNames[i][j] + '"');
				var row = data[3].length,
					dataSheetName = fileNames[i][j].split('.csv')[0];
				if (fileNames[i][j].indexOf('Articles') !== -1) {
					dataSheetName = fileNames[i][j].slice(11, -4);
					console.log('dataSheetName: ', dataSheetName);
				}
				data = _.map(data, function(value, index) {
					// If an original value contains commas and gets delimited when parsing,
					// the separated parts shall be joined back together
					if (value.length > row) {
						return [_.initial(value).join(), _.last(value)];
					} else {
						return value;
					}
				});
				csvData.push({name: dataSheetName, data: data});
				processArticlesData(i, j+1);
			});
			fs.createReadStream('./' + fileNames[i][j]).pipe(parser);
		} else {
			processDataSheet(i+1);
		}
	}

	// Function to process files starting with "Application Info" & "Mobile" (combining columns)
	function processOtherData(i, j) {
		if (j < fileNames[i].length) {
			var parser = parse({delimiter: ','}, function(err, data){
				console.log('Processing file: "' + fileNames[i][j] + '"');
				if (j === 0) {
					temp = data;
				} else {
					var alteredData = _.chain(data)
					.filter(function(value, index) {
						return index>1;
					}).each(function(value, index) {
						return value.shift();
					}).unshift(data[0], null)
					.value();
					temp = _.map(temp, function(value, index) {
						return value.concat(alteredData[index]);
					});
				}
				processOtherData(i, j+1);
			});
			fs.createReadStream('./' + fileNames[i][j]).pipe(parser);
		} else {
			csvData.push({name: fileNames[i][j-1].split(' - ')[0], data: temp});
			temp = [];
			processDataSheet(i+1);
		}
	}
	processDataSheet(0);
}

module.exports = createXLSX;