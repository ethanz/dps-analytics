Install
-------

First make sure you have installed the latest version of [node.js](http://nodejs.org/)
(You may need to restart your computer after this step).

From NPM for use as a command line app:

    npm install dps-analytics -g

From NPM for programmatic use:

    npm install dps-analytics

Usage
-------

* Sign into [Adobe® Digital Publishing Suite portal](https://digitalpublishing.acrobat.com/SignIn.html) and go to "Analytics" section
* Download all the csv files into one place.
* Open up terminal, and run `$ dps-analytics (optional: output file name)` in the folder where all the csv are.